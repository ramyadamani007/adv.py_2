class Bird():
	def intro (self):
		print ("There are more that a million of varities of birds.\n")
		
	def fly (self):
		print ("Almost every birds can fly but some cannot.\n")
		
class Parrot(Bird):
	def fly (self):
		print ("Parrot is a bird and it can fly\n")
		
class Penguin(Bird):
	def fly (self):
		print ("Penguin is a bird but cannot fly.\n")
		
a = Bird()
b = Parrot()
c = Penguin()

a.intro()
a.fly()

b.intro()
b.fly()

c.intro()
c.fly()
