class study():
	def __init__(self):
		self.course = "Python Programming Course"
		self.__ide = "Jupyter Notebook"		#Private variable
		
	def course_name (self):
		return self.course + ":" +self.__ide
		
obj = study()

print (obj.course)
print (obj._study__ide)					#Accessing Private variable
print (obj.course_name()) 
