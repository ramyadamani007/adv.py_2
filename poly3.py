class Bank ():
	def interest (self,p,r,t):
		self.r = r
		self.p = p
		self.t = t
	
	def calculate (self):
		return ((self.p * self.r * self.t)/100)
		
class HDFC (Bank):
	def interest (self):
		self.p = 5000
		self.r = 3
		self.t = 2
		
class SBI (Bank):
	def interest (self):
		self.p = 5000
		self.r = 5
		self.t = 2
		
class ICICI (Bank):
	def interest (self):
		self.p = 5000
		self.r = 6
		self.t = 2
		
a = HDFC()
b = SBI()
c = ICICI()

a.interest()
print("Interest amount in HDFC: ",a.calculate())

b.interest()
print("Interest amount in SBI: ",b.calculate())

c.interest()
print("Interest amount in ICICI: ",c.calculate())



























